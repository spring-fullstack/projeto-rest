package br.com.mrb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.mrb.model.User;
import br.com.mrb.repository.UserRepository;

@CrossOrigin("*")
@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserRepository userRepository; 
	
	@RequestMapping(value = "", method= RequestMethod.GET)
	private List<User> findAll() {
		return userRepository.findAll(); 
	}
	
	
	@RequestMapping(value = "", method= RequestMethod.POST)
	private User save(@RequestBody User user) {
		userRepository.save(user);
		return user;
	}
	
	@RequestMapping(value = "", method= RequestMethod.PUT)
	private User update(@RequestBody User user) {
		userRepository.save(user);
		return user;
	}
	
	@RequestMapping(value = "/{id}", method= RequestMethod.DELETE)
	private void delete(@PathVariable Long id) {
		userRepository.deleteById(id);
		
	}
	
}