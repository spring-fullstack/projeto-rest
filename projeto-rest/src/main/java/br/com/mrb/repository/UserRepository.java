package br.com.mrb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.mrb.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
